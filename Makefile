DIR := ${CURDIR}

-include $(DIR)/config.mk


MARIADB_CONTAINER := $(or $(MARIADB_CONTAINER),mariadb)
PORT := $(or $(PORT),8080)

DI_NAME := $(or $(DI_NAME),graphi/phpmyadmin)
DI_TAG := $(or $(DI_TAG),0.0.1)
DC_NAME := $(or $(DC_NAME),myadmin)

DI_ID := $(shell docker images | grep "$(DI_NAME)\s*$(DI_TAG)" | sed -n "s/[0-9a-z\/-]*\s*[0-9\.]*\s*\([0-9a-z]*\).*/\1/p")
DC_ID := $(shell docker ps -a | grep "$(DI_NAME):$(DI_TAG).*$(DC_NAME)" | sed -n "s/\([0-9a-z]*\).*/\1/p")
DC_STARTED_ID := $(shell docker ps | grep "$(DI_NAME):$(DI_TAG).*$(DC_NAME)" | sed -n "s/\([0-9a-z]*\).*/\1/p")


docker-start: docker-run
ifeq ($(DC_STARTED_ID),)
		docker start $(DC_NAME)
endif

docker-build:
ifeq ($(DI_ID),)
		docker build -t $(DI_NAME):$(DI_TAG) ./Docker
endif

docker-run: docker-build
ifeq ($(DC_ID),)
		docker run \
			--name $(DC_NAME) \
			-d \
			-e PMA_PORT=3307 \
			--link $(MARIADB_CONTAINER):db \
			-p $(PORT):80 $(DI_NAME):$(DI_TAG)
endif


docker-stop:
		docker stop $(DC_NAME)


docker-clean:
		-docker stop $(DC_NAME)
		-docker rm $(DC_NAME)
		docker rmi $(DI_NAME):$(DI_TAG)
